jQuery(function( $ ) {
	'use strict';


	plnodata();
	function plnodata() {
		if ($('#groupitems').children('tr').length < 1) {
			$('#groupitems').append('<tr><td class="nodata" colspan="4">No products found.</td></tr>')
		}
	}

	$('#psel').select2({
		placeholder: "Select product",
    	allowClear: true
	});

	function capitalizeFirstLetter(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	}

	// Add product
	$('.add-product').on("click", function (e) {
		e.preventDefault();
		let product_id = $(this).parent().find('#psel').val();
		let color = $(this).parent().find('input.color').val();
		let colorName = $(this).parent().find('input#colorname').val();

		if (product_id && color && colorName) {
			let title = $('#psel').children('option[value="' + product_id + '"]').text();
			$('#groupitems').children('tr[data-id="' + product_id + '"]').remove(); // remove item if exist

			let output = `<tr data-id="${product_id}">
				<td>#</td>
				<td>
					${title}
					<input type="hidden" name="products[${product_id}][id]" value="${product_id}" />
				</td>
				<td>
					${capitalizeFirstLetter(colorName)}
					<input type="hidden" name="products[${product_id}][colorname]" value="${colorName}" />
				</td>
				<td>
					<span class="colorshow" style="background-color: ${color}"></span>
					<input type="hidden" name="products[${product_id}][color]" value="${color}" />
				</td>
				<td><span class="removeitm">+</span></td>
			</tr>`;

			if ($('#groupitems').children('tr').children('.nodata').length > 0) {
				$('#groupitems').html(""); // If nodata make emty the holder
			}
			
			$('#groupitems').append(output); // append new item

			$('#groupitems').children('tr').each(function (ind, val) {
				$(this).children().first('td').text((ind + 1))
			});


			$('#psel').val(null).trigger('change'); // make empty after added
			$(this).parent().find('input.color').val("#000000"); // make empty after added
			$(this).parent().find('input#colorname').val(""); // make empty after added
		}
	});

	$(document).on('click', '.removeitm', function () {
		$(this).parents('tr').remove();
		plnodata();
	});

});
