<?php
global $wpdb, $product;
$product_id = $product->get_id();

$hasProduct = $wpdb->get_var("SELECT group_id FROM {$wpdb->prefix}productslinker WHERE product_id = $product_id");
if($hasProduct){ ?>
<div id="plgroup">
    <?php $pos = (get_option('layoutposition') ? get_option('layoutposition') : 'left'); ?>
    <h4 style="text-align: <?php echo $pos ?>" class="titleofcolors">CHOOSE YOUR COLOR</h4>
    <ul class="plitems">
    <?php
        global $readColor;
        $selected = get_option('layoutstyle');
        if($selected){
            switch ($selected) {
                case 'circle':
                    echo '<style>';
                    echo '
                    div#plgroup ul.plitems li {
                        border-radius: 50%;
                        width: 38px !important;
                        height: 38px !important;
                    }
                    div#plgroup ul.plitems li a{
                        border-radius: 50% !important;
                    }';
                    echo '</style>';
                    break;

                case 'squire':
                    echo '<style>';
                    echo '
                    div#plgroup ul.plitems li {
                        width: 50px;
                        height: 38px; 
                    }';
                    echo '</style>';
                    break;
                case '-1':
                    echo '<style>';
                    echo '
                    div#plgroup ul.plitems li {
                        width: 50px;
                        height: 38px; 
                    }';
                    echo '</style>';
                    break;
                default:

                    break;
            }
        }

        echo '<style>';
        echo 'div#plgroup ul.plitems {
            justify-content: '.$pos.';
        }';
        echo '</style>';
        $productItems = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}productslinker WHERE group_id = $hasProduct", ARRAY_A);
        if($productItems && is_array($productItems)){
            $layouts = get_option('layoutstyle');
            foreach($productItems as $item){
                $selected = $this->selected($item['product_id']);
                $itemproduct = wc_get_product( $item['product_id'] );
                $permalink = $itemproduct->get_permalink();
                if($layouts){
                    if($layouts === 'squire' || $layouts === 'circle' || $layouts === '-1'){
                        echo '<li class="'.$selected.'"><a style=" background: '.$item['color'].';" href="'.($selected ? '#' : $permalink).'"></a>
                        <span class="linkertooltip">'.ucfirst($item['colorname']).'</span>
                        </li>';
                    }else if($layouts === 'thumbnail'){
                        echo '<li class="imgitem '.$selected.'"><a href="'.($selected ? '#' : $permalink).'"><img src="'.get_the_post_thumbnail_url( $item['product_id'] ).'" alt="thumbnail"/></a>
                        <span class="linkertooltip">'.ucfirst($item['colorname']).'</span>
                        </li>';
                    }
                }
            }
        }
        ?>
    </ul>
</div>
<?php
}