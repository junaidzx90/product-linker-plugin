<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://www.fiverr.com/junaidzx90
 * @since      1.0.0
 *
 * @package    Products_Linker
 * @subpackage Products_Linker/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Products_Linker
 * @subpackage Products_Linker/includes
 * @author     junaidzx90 <admin@easeare.com>
 */
class Products_Linker_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
