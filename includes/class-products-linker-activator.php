<?php

/**
 * Fired during plugin activation
 *
 * @link       https://www.fiverr.com/junaidzx90
 * @since      1.0.0
 *
 * @package    Products_Linker
 * @subpackage Products_Linker/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Products_Linker
 * @subpackage Products_Linker/includes
 * @author     junaidzx90 <admin@easeare.com>
 */
class Products_Linker_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		global $wpdb;
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	
		$productslinker = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}productslinker` (
			`ID` INT NOT NULL AUTO_INCREMENT,
			`group_id` INT NOT NULL,
			`product_id`  INT NOT NULL,
			`color`  VARCHAR(55) NOT NULL,
			`colorname`  VARCHAR(55) NOT NULL,
			PRIMARY KEY (`ID`)) ENGINE = InnoDB";
			dbDelta($productslinker);
	}

}