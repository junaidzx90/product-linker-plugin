<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://www.fiverr.com/junaidzx90
 * @since      1.0.0
 *
 * @package    Products_Linker
 * @subpackage Products_Linker/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Products_Linker
 * @subpackage Products_Linker/admin
 * @author     junaidzx90 <admin@easeare.com>
 */
class Products_Linker_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Products_Linker_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Products_Linker_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		global $post;

		if(get_post_type( $post ) === 'products-group')
			wp_enqueue_style( 'linkerselect2', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css', array(), $this->version, 'all' );
			wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/products-linker-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Products_Linker_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Products_Linker_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		global $post;
		if(get_post_type( $post ) === 'products-group'){
			wp_enqueue_script( 'linkerselect2', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js', array( ), $this->version, false );
			wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/products-linker-admin.js', array( 'jquery' ), $this->version, false );
			wp_localize_script($this->plugin_name, "ajax", array(
				'ajaxurl' 	=> admin_url('admin-ajax.php'),
				'nonce' 	=> wp_create_nonce('ajax-nonce'),
			));
		}
	}

	function products_group(){
		$labels = array(
			'name'                  => __( 'Product Groups', 'products-linker' ),
			'singular_name'         => __( 'groups', 'products-linker' ),
			'menu_name'             => __( 'Product Groups', 'products-linker' ),
			'name_admin_bar'        => __( 'groups', 'products-linker' ),
			'add_new'               => __( 'New group', 'products-linker' ),
			'add_new_item'          => __( 'New group', 'products-linker' ),
			'new_item'              => __( 'New group', 'products-linker' ),
			'edit_item'             => __( 'Edit group', 'products-linker' ),
			'view_item'             => __( 'View group', 'products-linker' ),
			'all_items'             => __( 'All groups', 'products-linker' ),
			'search_items'          => __( 'Search groups', 'products-linker' ),
			'parent_item_colon'     => __( 'Parent groups:', 'products-linker' ),
			'not_found'             => __( 'No groups found.', 'products-linker' ),
			'not_found_in_trash'    => __( 'No groups found in Trash.', 'products-linker' )
		);     
		$args = array(
			'labels'             => $labels,
			'description'        => 'groups custom post type.',
			'public'             => true,
			'publicly_queryable' => false,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'products-group' ),
			'capability_type'    => 'post',
			'has_archive'        => false,
			'hierarchical'       => false,
			'menu_position'      => 45,
			'menu_icon'      	 => 'dashicons-networking',
			'supports'           => array( 'title' ),
			'show_in_rest'       => false
		);
		  
		register_post_type( 'products-group', $args );
	}

	function products_linker_admin_menu(){
		add_submenu_page( 'edit.php?post_type=products-group', 'Settings', 'Settings', 'manage_options', 'settings', [$this, 'product_linker_settings'], null );
		// options
		add_settings_section( 'plinker_settings_section', '', '', 'plinker_settings_page' );

		// Category
		add_settings_field( 'layoutstyle', 'Layout', [$this, 'layoutstyle_cb'], 'plinker_settings_page', 'plinker_settings_section');
		register_setting( 'plinker_settings_section', 'layoutstyle');
		// Category
		add_settings_field( 'layoutposition', 'Position', [$this, 'layoutposition_cb'], 'plinker_settings_page', 'plinker_settings_section');
		register_setting( 'plinker_settings_section', 'layoutposition');
	}

	function layoutstyle_cb(){
		$selected = get_option('layoutstyle');
		echo '<select name="layoutstyle" class="widefat">
		<option value="-1">Select layout</option>
		<option '.($selected === 'circle' ? 'selected' : '').' value="circle">Circle</option>
		<option '.($selected === 'squire' ? 'selected' : '').' value="squire">Squire</option>
		<option '.($selected === 'thumbnail' ? 'selected' : '').' value="thumbnail">Image</option>
		</select>';
	}

	function layoutposition_cb(){
		echo '<select name="layoutposition">
			<option '.(get_option('layoutposition') == "left" ? "selected" : "").' value="left">Left</option>
			<option '.(get_option('layoutposition') == "center" ? "selected" : "").' value="center">Center</option>
			<option '.(get_option('layoutposition') == "right" ? "selected" : "").' value="right">Right</option>
		</select>';
	}

	function product_linker_settings(){
		echo '<h3>Settings</h3><hr>';
		echo '<div class="plinker_content">';
		echo '<form style="width: 50%" method="post" action="options.php">';
		echo '<table class="widefat">';
		settings_fields( 'plinker_settings_section' );
		do_settings_fields( 'plinker_settings_page', 'plinker_settings_section' );
		echo '</table>';
		submit_button();
		echo '</form>';
		echo '</div>';
	}

	function overrides_predefined_txts($translation, $text, $domain){
		global $post;
		if (get_post_type( $post ) == 'products-group') {
			if ( $text == 'Publish' )
				return 'Create';
			if ( $text == 'Update' )
				return 'Update group';
			if ( $text == 'Post updated.' )
				return 'Group Updated.';
			if ( $text == 'Post published.' )
				return 'Group Created.';
			if ( $text == 'Add title' )
				return 'Group name';
			return $translation;
		}
		
		return $translation;
	}
	
	// Remove Quick edit products-group posts
	function remove_quick_edit_products_group( $actions, $post ) {
		if(get_post_type( $post ) === 'products-group'){
			unset($actions['inline hide-if-no-js']);
			return $actions;
		}else{
			return $actions;
		}
   	}
	   
	//  Remove edit option from bulk
	function remove_products_group_edit_actions( $actions ){
		unset( $actions['edit'] );
		return $actions;
   	}
	
	// Manage table columns
	function manage_products_group_columns($columns) {
		unset(
			$columns['title'],
			$columns['date']
		);
	
		$new_columns = array(
			'title' => __('Group name', 'member-posts'),
			'products' => __('Products Using', 'member-posts'),
			'date' => __('Created Date', 'member-posts'),
		);
	
		return array_merge($columns, $new_columns);
	}
	
	// View custom column data
	function manage_products_group_columns_views($column_id, $post_id){
		global $wpdb;
		switch ($column_id) {
			case 'products':
				$counts = $wpdb->query("SELECT * FROM {$wpdb->prefix}productslinker WHERE group_id = $post_id");
				if($counts){
					if($counts > 1){
						echo $counts. ' Products';
					}else{
						echo $counts. ' Product';
					}
				}
				break;
			
			default:
				# code...
				break;
		}
	}
	
	// Manage sortable column
	function manage_products_group_columns_sortable($columns){
		$columns['status'] = 'Status';
		$columns['products'] = 'Products Using';
		return $columns;
	}

	// Meta boxes
	function pg_meta_boxes(){
		global $wp_meta_boxes;
		unset($wp_meta_boxes['products-group']);

		add_meta_box( 'submitdiv', 'Save Group', 'post_submit_meta_box', 'products-group', 'side' );
		add_meta_box( 'groupdata', 'Group Items', [$this, 'products_group_box'], 'products-group', 'advanced' );
	}

	// Group box
	function products_group_box($post){
		global $wpdb;
		?>
		<div class="groupbox">

			<div class="product-select-inp">

				<select id="psel">
					<option></option>
					<?php
					$args = array(
						'post_type'      => 'product',
						'posts_per_page' => -1,
						'post_status'	 => 'publish',
						'orderby'		 => 'date',
						'order'          => 'DESC'
					);
					$products = get_posts($args);
					if($products && sizeof($products) > 0){
						foreach($products as $product){
							if(!$wpdb->get_var("SELECT ID FROM {$wpdb->prefix}productslinker WHERE product_id = {$product->ID}")){
								echo '<option value="'.$product->ID.'">'.$product->post_title.'</option>';
							}
						}
					}
					?>
				</select>
				<input type="text" id="colorname" placeholder="Color name" name="colorname">
				<input type="color" class="color" value="#000000">
				<button class="button-secondary add-product">Add</button>
			</div>

			<table class="widefat grouptbl">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Color Name</th>
						<th>Color</th>
						<th>#</th>
					</tr>
				</thead>
				<tbody id="groupitems">
					<?php 
					$productItems = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}productslinker WHERE group_id = {$post->ID}",ARRAY_A);

					if($productItems && is_array($productItems)){
						$sel = 1;
						foreach($productItems as $item){
							?>
							<tr data-id="<?php echo $item['product_id'] ?>">
								<td><?php echo $sel ?></td>
								<td>
									<?php
									$product = wc_get_product( $item['product_id'] );
									if($product){
										echo $title = $product->get_title();
									}
									?>
									<input type="hidden" name="products[<?php echo $item['product_id'] ?>][id]" value="<?php echo $item['product_id'] ?>" />
								</td>
								<td>
									<?php echo ucfirst($item['colorname']) ?>
									<input type="hidden" name="products[<?php echo $item['product_id'] ?>][colorname]" value="<?php echo $item['colorname'] ?>" />
								</td>
								<td>
									<span class="colorshow" style="background-color: <?php echo $item['color'] ?>"></span>
									<input type="hidden" name="products[<?php echo $item['product_id'] ?>][color]" value="<?php echo $item['color'] ?>" />
								</td>
								<td><span class="removeitm">+</span></td>
							</tr>
							<?php
							$sel++;
						}
					}
					?>
					<!--  -->
				</tbody>
			</table>
		</div>
		<?php
	}

	// Save save_products_group
	function save_products_group($post_id){
		global $wpdb;
		$wpdb->query("DELETE FROM {$wpdb->prefix}productslinker WHERE group_id = $post_id");
		if(isset($_POST['products'])){
			$groupItems = $_POST['products'];
			$groupItems = array_values($groupItems);
			
			if(sizeof($groupItems) > 0){
				foreach($groupItems as $item){
					$product_id = intval($item['id']);
					$color = $item['color'];
					$colorname = $item['colorname'];

					$hashItem = $wpdb->get_var("SELECT ID FROM {$wpdb->prefix}productslinker WHERE product_id = $product_id AND group_id = $post_id");
					
					$wpdb->insert($wpdb->prefix.'productslinker',
					array(
						'group_id' => $post_id,
						'product_id' => $product_id,
						'color' => $color,
						'colorname' => $colorname
					), array('%d', '%d', '%s', '%s'));
				}
			}
		}
	}
}